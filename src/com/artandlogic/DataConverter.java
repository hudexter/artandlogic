package com.artandlogic;

public class DataConverter extends DataConverterTest {

    public String Encode(short input) {

        short temp = (short) (input + 8192);

        // get the hexdecimal string of high byte
        byte highByte = (byte) ((temp >> 7) & 0xFF);
        String highByteString = String.format("%02X", highByte);

        // get the hexdecimal string of low byte
        byte lowByte = (byte) (temp & 0x7F);
        String lowByteString = String.format("%02X", lowByte);

        return highByteString + lowByteString;
    }

    public short Decode(String s) {
        int len = s.length(); // len is actually 4

        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) { // convert hexadecimal string to 2 bytes
            int high4BitsInInteger = Character.digit(s.charAt(i), 16) << 4;
            int low4BitsInInteger = Character.digit(s.charAt(i + 1), 16);
            data[i / 2] = (byte) (high4BitsInInteger + low4BitsInInteger);
        }

        // decode to original value
        byte highByte = data[0];
        byte lowByte = data[1];
        return (short) ((highByte << 7 | lowByte & 0x007F) - 8192);
    }

}
