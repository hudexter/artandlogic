package com.artandlogic;

import java.util.*;

public class Main {

    private static boolean is4CharHexNumber(String cadena) {
        try {
            if (cadena == null || cadena.length() != 4)
                return false;
            Long.parseLong(cadena, 16);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    private static boolean is14BitSignedInteger(String cadena) {

        try {
            short input = Short.parseShort(cadena, 10);
            if (input >= -8192 && input <= 8191)
                return true;
        } catch (NumberFormatException ex) {
            return false;
        }
        return false;
    }

    public static void main(String[] args) {
        DataConverter dataConverter = new DataConverter();
        Scanner scanner = new Scanner(System.in);
        try {
            while (true) {
                System.out.println("Please enter input type (1 or 2), 1 is integer, 2 is hexadecimal string:");
                String input = scanner.nextLine();
                if (input.equalsIgnoreCase("1")) {
                    System.out.println("Please enter an integer [-8192, 8191]:");
                    input = scanner.nextLine();
                    if (is14BitSignedInteger(input)) {
                        short value = Short.parseShort(input, 10);
                        System.out.format("%s is encoded to %s.\n", input, dataConverter.Encode(value));
                    } else {
                        System.out.println("Invalid input: integer must be between -8192 and 8191.");
                    }
                } else if (input.equalsIgnoreCase("2")) {
                    System.out.println("Please enter a hexadecimal string with 4 characters with:");
                    input = scanner.nextLine();
                    if (is4CharHexNumber(input)) {
                        System.out.format("%s is decoded to %s.\n", input, dataConverter.Decode(input));
                    } else {
                        System.out.println("Invalid input: hexadecimal string must be of 4 characters.");
                    }

                } else {
                    System.out.println("Input type is invalid.");
                }
            }
        } catch (IllegalStateException | NoSuchElementException e) {
            System.out.println("Exiting");
        }
    }
}
