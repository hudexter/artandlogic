package com.artandlogic;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DataConverterTest {

    @Test
    @DisplayName("Should return the correct hexdecimal string")
    public void Encode_GivenValidInput_ShouldReturnCorrectHexString() {
        DataConverter converter = new DataConverter();
        assertEquals("4000", converter.Encode((short) 0));
        assertEquals("0000", converter.Encode((short) -8192));
        assertEquals("7F7F", converter.Encode((short) 8191));
        assertEquals("5000", converter.Encode((short) 2048));
        assertEquals("2000", converter.Encode((short) -4096));
    }

    @Test
    @DisplayName("Should return the original integer")
    public void Decode_GivenValidInput_ShouldReturnCorrectShortInteger() {
        DataConverter converter = new DataConverter();
        assertEquals((short) 0, converter.Decode("4000"));
        assertEquals((short) -8192, converter.Decode("0000"));
        assertEquals((short) 8191, converter.Decode("7F7F"));
        assertEquals((short) 2048, converter.Decode("5000"));
        assertEquals((short) -4096, converter.Decode("2000"));
        assertEquals((short) -6907, converter.Decode("0A05"));
        assertEquals((short) 2688, converter.Decode("5500"));
    }
}
