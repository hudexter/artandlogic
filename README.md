## Introduction

This task is finished in about 1.5 hours. This zip file contains 
* DataCoverter, DataConverterTest, and Main program
* The ConvertedData.txt, with encoded or decoded results for inputs required from pdf.

## Setup

This project requires Java

* Run from command line inside folder `out/production/DataConverter`

```
java com.artandlogic.Main
```
It will prompt you to enter input type, 1 is for integer, 2 is for hexadecimal string.

* You can also run it as a project in IntelliJ IDEA

## Testing

* To run unit tests, open this project in IntelliJ IDEA, and run tests in DataConverterTest, as shown in TestScreenShot.png